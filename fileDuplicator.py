#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 23 20:53:32 2021

@author: SnowFox
"""

# Todo:
# Replace os.access with try/except clauses (Timing attack security bug fix).


import os
import traceback
import argparse
import logging as log
from pathlib import Path
import PySimpleGUI as sg
import fileByteWriter as fw


def fileDup(sourcepath, targetpath):
    """
    Duplicates a source top-down at the given target destination.
    Logs directory creation, file writes, and errors to filesSyncLog.txt.
    :param sourcepath: Full path to source (what to copy).
    :param targetpath: Full path to target (where to copy).
    """
    path = sourcepath
    backupPath = targetpath
    log.basicConfig(filename='filesSyncLog.txt', filemode='w',
                    level=log.DEBUG, format='%(levelname)s %(message)s')
    if not os.access(path, os.F_OK) and os.access(backupPath, os.W_OK):
        log.critical(f'Source {path} and target {backupPath} cannot be accessed. Exiting.')
        exit()
    log.info(f'Source: {path}')
    log.info(f'backupPath: {backupPath}')
    totalFiles = 0
    totalDirectories = 0
    print('Scanning now')
    try:
        for root, dirs, files in os.walk(path, topdown=True):
            # Creates directory structure topdown.
            # Allows for async/threading/etc later on.
            for directory in dirs:
                targetRoot = Path(str(root).replace(str(path), str(backupPath)))
                targetDirPath = Path(f'{targetRoot}/{directory}')
                if not os.access(targetDirPath, os.F_OK):
                    log.info(f'Creating {targetDirPath}')
                    os.mkdir(Path(targetDirPath))
                    totalDirectories += 1
            # Copies files to directory structure topdown.
            print(f'Copying files in {root} now.')
            for file in files:
                sourceFilePath = Path(f'{root}/{file}')
                targetFilePath = Path(f'{str(root).replace(path, backupPath)}/{file}')
                # targetFilePath = Path(str(sourceFilePath).replace(str(path), str(backupPath)))
                if not os.access(targetFilePath, os.F_OK):
                    fw.fileTransfer(sourceFilePath, targetFilePath)
                    log.info(f'\t{totalFiles}::{sourceFilePath} --> {targetFilePath}')
                    totalFiles += 1
    except FileNotFoundError:
        log.critical(f'Error: {traceback.print_exc()}')
        exit()
    dirTotal = f"\nTotal directories synced is {totalDirectories}."
    filesTotal = f"Which consist of {totalFiles-1} files synced."
    log.info(dirTotal)
    log.info(filesTotal)
    print(dirTotal)
    print(filesTotal)


if __name__ == "__main__":
    try:
        # GUI or cli source & target selection.
        parser = argparse.ArgumentParser()
        parser.add_argument("--source",
                            help="Full path to disk, mountpoint, directory, share, file, etc (What to copy).")
        parser.add_argument("--target",
                            help="Full path to disk, mountpoint, directory, share (Where to copy).")
        args = parser.parse_args()

        if args.source and args.target:
            path = args.source
            backupPath = args.target
        else:
            path = sg.popup_get_folder('Select source to copy')
            backupPath = sg.popup_get_folder('Select target to copy source to')
        if path and backupPath:
            fileDup(path, backupPath)
        else:
            print('source and target not selected. Exiting.')
    except Exception as e:
        log.critical(f'Error {e} - {traceback.print_exc()}')
        exit()
