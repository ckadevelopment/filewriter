#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 24 22:57:16 2021

@author: SnowFox
"""
import os
from time import perf_counter
from pathlib import Path
import zlib
import click


def crc32(file, buffer=24000):
    if os.access(file, os.R_OK):
        checksum = 0
        with open(file, 'rb') as f:
            while buffData := f.read(buffer):
                if buffData:
                    checksum += zlib.crc32(buffData)
        return checksum

def targetAddExtension(file):
    filename = Path(f'{file}.fdupart')
    return filename

def fileChecker(file1, file2):
    try:
        print(f'Generating checksum on source-file {file1}.')
        checksum1 = crc32(file1)
        print(f'Generating checksum on target-file {file2}.')
        checksum2 = crc32(file2)
        if checksum1 == checksum2:
            fcheck = 'Success'
            return fcheck
        else:
            fcheck = f'Fail\n{file1} :: {checksum1}\n{file2} :: {checksum2}'
            return fcheck
    except Exception as e:
        return 'Fail', f'error: {e}'

def fileTransfer(source, target):
    sourceSize = os.stat(source).st_size
    targetPart = targetAddExtension(target)
    targetSize = 0
    bufferSize = 8000 * 3
    MBCONV = 0.00000095367432  # binary conversion byte to megabyte.
    with open(source, "rb") as sourceFile:
        fstart = perf_counter()  # Start time of file transfer.
        fend = 0                 # Finish time of file transfer.
        with open(targetPart, "ab") as targetFile:
            interimStart = perf_counter()  # Start of sample period.
            counter = targetSize  # Tracks bytes written to targetFile per sample period.
            while targetSize < sourceSize:
                targetSize += targetFile.write(sourceFile.read(bufferSize))
                interimEnd = fend = perf_counter()  # End of sample period.
                if interimEnd - interimStart >= 1.0:
                    click.clear()
                    # Transfer speed in MBps.
                    interimTotal = ((targetSize - counter) * MBCONV) / (
                        interimEnd - interimStart
                    )
                    interimStart = interimEnd
                    counter = targetSize
                    click.echo(
                        f"Transferring {target}\n"
                        f"{targetSize} of {sourceSize}  {targetSize/sourceSize*100:.2f}%  "
                        f"{interimTotal:.2f} MB/s"
                    )
        click.clear()
        fileCheck = fileChecker(source, targetPart)
        if 'Success' == fileCheck:
            os.rename(targetPart, target)
        click.echo(
            f"average {(targetSize * MBCONV) / (fend - fstart):.2f} MB/s  "
            f"({targetSize * MBCONV:.1f} MB in {fend - fstart:.2f} seconds)\n"
            f"File verification: {fileCheck}"
        )


if __name__ == '__main__':
    fileSource = Path("Viy.mp4")
    fileTarget = Path("tmp/Viy.mp4")
    fileTransfer(fileSource, fileTarget)
