from tkinter import *
from tkinter.ttk import *
from tkinter import ttk
import threading
import queue
from random import seed, randrange
import time

root = Tk()
root.title('Demo')
root.geometry("650x400")
root.config(background='#575758')

ttk.Treeview
my_progress = ttk.Progressbar(root, orient=HORIZONTAL,
                              length = 400, mode="determinate")
Filelist = []
total_filesize = 0
Processed_filesize = 0

def step():
    for work in Filelist:
        for x in range(100):
            work[2]['value'] += 1
            work[3].config(text=(str(work[2]['value'])+'%'))
            root.event_generate("<<Update>>", when="tail")
            time.sleep(.01)

def stop():
    my_progress.stop()
    my_progress['value'] = 0
    Filelist = []
    yield

def updateMain(*args):
    #calculates overall progress for main progress bar
    process_done = 0
    total_filesize = 0
    for file in Filelist:#[filename,filesize,progressbar]
        #you would divide 2 by 45% or .45
        filesize = int(file[1]['text'])
        processed = float(int(file[2]['value'])/100) #changes percentage to decimal
        total_processed = round(filesize*processed)
        process_done = process_done + total_processed
        total_filesize = total_filesize + filesize

    #update the main progressbar and update window
    percentage_done = round((process_done/total_filesize) * 100)
    my_progress['value'] = percentage_done
    my_label.config(text=f'{percentage_done}%')
    root.update()

def threading_start():
    thread = threading.Thread(target=step)
    thread.daemon = True
    thread.start()

def getWork():
    # blue -  #289ff7
    # Grey -  #cacbcd
    color =0
    for work in range(20):
        nrow = work + 3 #places below the main progress and buttons
        if color == 0:  # blue
            filename = "File"+work.__str__()
            new_label = Label(root, text=f'file{work}',background='#289ff7')
            new_label.grid(row=nrow, column=0,sticky='ew', columnspan=3)
            filesize = randrange(10000000)
            new_filesize = Label(root, text=f'{filesize}',background='#289ff7')
            new_filesize.grid(row=nrow, column=1)
            New_ProgressBar = ttk.Progressbar(root, orient=HORIZONTAL,
                                              length=100, mode="determinate")
            New_ProgressBar.grid(row=nrow, column=3)
            percentage_label = Label(root, text=(str(New_ProgressBar['value']) + '%'),background='#289ff7')
            percentage_label.grid(row=nrow, column=4)
            Fileinfo = [new_label, new_filesize, New_ProgressBar, percentage_label]  # [filename,filesize,progressbar]
            Filelist.append(Fileinfo)
            color += 1

        else:
            filename = "File" + work.__str__()

            new_label = Label(root, text=f'file{work}', background='#cacbcd')
            new_label.grid(row=nrow, column=0,sticky='ew', columnspan=3)
            filesize = randrange(10000000)
            new_filesize = Label(root, text=f'{filesize}', background='#cacbcd',anchor=CENTER)
            new_filesize.grid(row=nrow, column=1)
            New_ProgressBar = ttk.Progressbar(root, orient=HORIZONTAL,
                                              length=100, mode="determinate")
            New_ProgressBar.grid(row=nrow, column=3)
            percentage_label = Label(root, text=(str(New_ProgressBar['value']) + '%'), background='#cacbcd')
            percentage_label.grid(row=nrow, column=4)
            Fileinfo = [new_label, new_filesize, New_ProgressBar, percentage_label]  # [filename,filesize,progressbar]
            Filelist.append(Fileinfo)

            color -= 1

    threading_start()

root.bind("<<Update>>", updateMain)
scrollbar = ttk.Scrollbar(root,orient='vertical')

scrollbar.grid(column=5,rowspan=10)
my_progress.grid(row=0,column=1, pady=20)
my_button = Button(root,text='Start', command=getWork)
my_space1 = Label(root,text="Completion:", background='#575758')
my_space1.grid(row=0,column=0,pady=20)
my_button.grid(row=1, column=1)
my_button2 = Button(root, text='stop', command=stop)
my_button2.grid(row=1, column=0)
my_label = Label(root, text="")
my_label.grid(row=0, column=1)
grid_subName = Label(root, text='File name', background='#575758')
grid_subName.grid(row=2,column=0)
grid_subsize = Label(root, text='File Size', background='#575758')
grid_subsize.grid(row=2,column=1)

root.mainloop()
