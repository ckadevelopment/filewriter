# fileWriter

Duplicates a source (disk, mountpoint, folder, share, or file) to a given target destination (disk, directory, mountpoint, etc).  
Files are crc32 checksum verified.  
Can be run directly or loaded as modules.

### Run application w/ GUI:  
~~~bash
$ python fileDuplicator.py
~~~
* Default way to run application.
* Application will run and 2 popups will show to select the source and target paths.
* Source is what you want to copy.
* Target is where you want to copy it.

### Run application w/ cli:
~~~bash
$ python fileDuplicator.py --source <path-to-source> --target <path-to-target>
~~~
* Source is what you want to copy.  
* Target is where you want to copy it.  

Log is created in the application directory as filesSyncLog.txt.  
All directory creations and file writes are logged.  
